<h1>Epic STUN server and P2P Client stuff</h1>
<h3>intro: this is a STUN server where every STUN protocol is made from scratch using only c++ boost::asio</h3>
<br><br>
functionality:<br>
>BINDING:<br>
>>binding (request, indication, success and error)<br>
>>mapped_address (construction and analysis tools, but not used by stun_server impl.)<br>
>>xor_mapped_address (returned by server after binding success header)<br>
>ERROR:<br>
>>error_code attribute (bad_request, unknown_attribute, server_error)<br>
>>unknown_attributes attribute (returned by stun_server when appropriate)<br>
all forementioned functionality has implemented factory and analysis tools (stun_frame and stun_frame_factory)<br>
as well as being fully implemented in the stun_server<br>
<br>
missing functionality for simple STUN server (RFC 5389):<br>
>fingerprint (optional and not recommended)<br>
>TCP-TLS support (not implemented, although TLS is mostly pointless for simple STUN)<br>
<br>
missing functionality for full STUN server (RFC 5389):<br>
>alot of stuff<br>
<br>
Future plans for chat client:<br>
<br>
Update and improve the html and javascript so that sent messages pops up in bot browser and it says who sent the message.<br>
Similar to messenger in facebook.<br>
Fix the fact that you have to manually copy and paste the remote and localDescriptor when connecting a peer.<br>
<br>
Future plans for webcam client:<br>
<br>
As of now, the webcam client uses google's stun server for some reason<br>
even though our localhost stun server is specified.<br>
Have tried to debug this but ran out of time. We want to fix this issue in the future.<br>
We also want to improve the html and css because it looks horible,<br>
this is because html and css have been a very low priority on this task.<br>
<br>
External deppendencies in both clients:<br>
<br>
Asio is used in the chat client to handle nettworking.<br>
socket.io is used in the webcam client to instantiate a connection between peers.<br>
<br>
Installation requierments: <br>
Node needs to be installed on your computer.<br>
<br>
Instructions to test the chat client:<br>
1. Start the stun and http server-<br>
2. In a broswer, accses localhost:8080, this tab will act as client A.<br>
3. Open the console in client A and copy the localDescription.<br>
4. In a new tab, accsess localhost:8081, this will act client B.<br>
5. When prompted in client B for remoteDescription, paste the localDescription from client A.<br>
6. Open the consloe in client B and copy the localDescription.<br>
7. In client A, press "Add remoteDescription, An paste client B's <br>
<br>
Instructions to test the webcam client:<br>
<br>
1. In a terminal, cd to node_webcam.<br>
2. Write the command "node server.js".<br>
3. in a browser, accses localhost:4000/broadcaster.html , This tab will stream video and audio to other connected peers.<br>
4. In an new tab, accses localhost:4000 , Here you will see the strem.<br>
<br>

Sources and inspiration: <br>
<br>
https://dev.to/nilmadhabmondal/let-s-build-a-video-chat-app-with-javascript-and-webrtc-380b <br>
https://tsh.io/blog/how-to-write-video-chat-app-using-webrtc-and-nodejs/ <br>
https://scotch.io/tutorials/build-a-video-chat-service-with-javascript-webrtc-and-okta <br>
https://www.youtube.com/watch?v=ieBtXwHvoNk <br>
https://gabrieltanner.org/blog/webrtc-video-broadcast<br>



