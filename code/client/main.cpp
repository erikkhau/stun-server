#include "http_server.hpp";

asio::io_context context;
unsigned short client_A_port = 8080;
unsigned short client_B_port = 8081;

int main() {
	std::thread clientA([&]() {
		http_server clientA(client_A_port, context);

		clientA.accept();
		});

	std::thread clientB([&]() {
		http_server clientB(client_B_port, context);

		clientB.accept();
		});

	std::thread contextThread([&]() {
		context.run();
		});

	if (clientA.joinable())
		clientA.join();

	if (clientB.joinable())
		clientB.join();

	if (contextThread.joinable())
		contextThread.join();
}