#include <asio.hpp>
#include <iostream>
#include <fstream>

class http_server {
	asio::io_context& context;
	asio::ip::tcp::acceptor acceptor;
	unsigned short& port;

public:

	http_server(unsigned short& port, asio::io_context& context) :
		context(context),
		port(port),
		acceptor(context, asio::ip::tcp::endpoint(asio::ip::tcp::v4(), port))
	{
		accept();
	}

	void accept() {
		asio::ip::tcp::socket sock(context);
		acceptor.accept(sock);
		std::cout << "Accepted connection from: " << port << std::endl;
		sock.wait(sock.wait_read);
		std::vector<char> in(sock.available());
		sock.read_some(asio::buffer(in, in.size()));

		handle_http(in, sock);
	}

private:

	std::string read_html() {
		std::ifstream inFile;
		if (port == 8080)
			inFile.open("clientA.html", std::ifstream::in);
		else if (port == 8081)
			inFile.open("clientB.html", std::ifstream::in);
		else return "";

		std::string htmlFile;
		while (inFile.good())
			htmlFile += inFile.get();

		inFile.close();

		return htmlFile;
	}

	void handle_http(std::vector<char> in, asio::ip::tcp::socket& sock) {
		std::string inString(in.begin(), in.end());

		asio::error_code ignored_error;

		std::string html = read_html();

		asio::write(sock, asio::buffer(html), ignored_error);

		sock.close();

		accept();
	}
};