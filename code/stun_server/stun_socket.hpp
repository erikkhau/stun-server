#pragma once

#include "s_deque.hpp"
#include "s_.hpp"

/*
* stun_socket:
* 
* * reading
* reads everything from the remote endpoint defined by listen()
* and stores it in m_messages until pop_message() is called
* at which point one message is read and discarded i.e. popped
* OR
* passes newly read data to message_handler function ptr
* 
* * listening:
* if listen is not called before send, socket will listen to send destination i.e. "dst"
* 
*/

class stun_socket {
	asio::io_context& m_context;
	asio::ip::udp::socket m_sock;
	std::vector<byte> m_rdata;

	s_deque<std::vector<byte>> m_messages;

	std::shared_mutex m_sock_mtx;

	std::function<void(const std::vector<byte>&, const asio::ip::udp::endpoint&)> message_handler;

public:
	stun_socket(asio::io_context& context, const asio::ip::udp::endpoint& p) :
		m_context(context),
		m_sock(context, p),
		m_rdata(1024U, NULL)
	{
		read();
	}
	stun_socket(asio::io_context& context) :
		m_context(context),
		m_sock(context),
		m_rdata(1024U, NULL)
	{
		
	}
	/* closes port i.e. stops listening for input */
	void close() {
		m_sock.close();
	}
	/* deletes saved input */
	void clear() {
		m_messages.clear();
		//flushes all cached data just in case
		memset(m_rdata.data(), NULL, m_rdata.size());
	}

	/* handle messages directly */
	void on_message(std::function<void(const std::vector<byte>&, const asio::ip::udp::endpoint&)> message_handler) {
		this->message_handler = message_handler;
	}

	/* reads data from endpoint */
	void listen(const asio::ip::udp::endpoint& p) {
		write_lock wlk(m_sock_mtx);

		asio::error_code ec;

		if (m_sock.is_open()) m_sock.close();
		m_sock.open(p.protocol());
		m_sock.bind(p, ec);

		if (ec) {
			std::cout << ec.message() << std::endl;
			throw "no bueno";
		}

		wlk.unlock();
		read();
	}
	/* sends data to dst. If socket was not open (e.g. first send) it also listens for data from dst */
	void send(const std::vector<byte>& data, const asio::ip::udp::endpoint& dst) {
		read_lock rlk(m_sock_mtx);

		if (!m_sock.is_open()) {
			m_sock.connect(dst);
			rlk.unlock();
			listen(m_sock.local_endpoint());
			rlk.lock();
		}

		std::shared_ptr<std::vector<byte>> dcpy = std::make_shared<std::vector<byte>>(data);
		m_sock.async_send_to(asio::buffer(*dcpy), dst,
			[dcpy](const asio::error_code& ec, size_t bytes) {
				if (ec) std::cout << ec.message();
			}
		);
	}
	std::vector<byte> pop_message(bool block_until_has_data = false) {
		if (message_handler && block_until_has_data) throw "message_handler exists & blocking => potential eternal blocking";	//TODO make DEBUG only

		return m_messages.pop_front(block_until_has_data);
	}
	asio::ip::udp::endpoint local_endpoint() {
		read_lock rlk(m_sock_mtx);

		return m_sock.local_endpoint();
	}


private:
	asio::ip::udp::endpoint last_src;
	void read() {
		read_lock rlk(m_sock_mtx);
		if (m_sock.is_open())
			m_sock.async_receive_from(asio::buffer(m_rdata, m_rdata.size()), last_src, [&](const asio::error_code& ec, size_t bytes) { read_handle(ec, bytes); });
	}
	void read_handle(const asio::error_code& ec, size_t bytes) {
		if (ec) {
			std::cout << ec.message() << std::endl;
		}
		if (bytes > 0) {
			auto itr = m_rdata.begin();

			if (!message_handler)
				m_messages.emplace_back(itr, itr + bytes);
			else
				message_handler(std::vector<byte>(itr, itr + bytes), last_src);

			//memset(m_rdata.data(), NULL, m_rdata.size());	not necessary
		}
		read();
	}
};
