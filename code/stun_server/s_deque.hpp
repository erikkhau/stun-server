#pragma once

#include <vector>
#include <deque>

#include <shared_mutex>
#include <condition_variable>

#include <iostream>
#include <chrono>
#include <functional>

#include <asio.hpp>
#include <asio/ts/buffer.hpp>
#include <asio/ts/internet.hpp>

using read_lock		= std::shared_lock<std::shared_mutex>;
using write_lock	= std::unique_lock<std::shared_mutex>;

using byte = uint8_t;

/*
* s_deque:
* thread safe deque wrapper
*/

template <typename T>
class s_deque {
	std::deque<T> q;

	std::shared_mutex mtx;
	std::condition_variable_any cv;

public:
	s_deque() : q() {

	}

	/*
	* READ
	*/

	bool empty() {
		read_lock rlk(mtx);

		return q.empty();
	}
	T peek_front() {
		read_lock rlk(mtx);

		return q.front();
	}
	size_t size() {
		read_lock rlk(mtx);

		return q.size();
	}
	bool contains(T o) {
		for (T l : q)
			if (l == o) return true;
		return false;
	}

	/*
	* WRITE
	*/

	T pop_front(bool block_until_has_data = false) {
		write_lock wlk(mtx);
		if (block_until_has_data)
			cv.wait(wlk, [&]() { return !q.empty(); });

		T o = q.front();
		q.pop_back();
		return o;
	}
	void push_back(const T& o) {
		write_lock wlk(mtx);

		q.push_back(o);

		wlk.unlock();
		cv.notify_one();
	}
	template <typename... Ts>
	void emplace_back(Ts&&... args) {
		write_lock wlk(mtx);

		q.emplace_back(std::forward<Ts>(args)...);

		wlk.unlock();
		cv.notify_one();
	}
	void clear() {
		write_lock wlk(mtx);

		q.clear();
	}
};
