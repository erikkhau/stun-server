#pragma once

#include <string>
#include <vector>

#include <stdlib.h>
#include <time.h>

#include "STUN_ENUMS.hpp"

using byte = uint8_t;

/* inserts primitive data into byte vector */
/* NB: only use primitive types e.g. don't use containers as "o" */
template<typename T>
void push_into_end_of_byte_vector(std::vector<byte>& v, const T& o) {
	byte* begin = (byte*)&o;
	byte* end = (byte*)(&o + 1);
	v.insert(v.end(), begin, end);
}

/* return true if system architecture uses big endian */
bool system_has_big_endian() {
	union {
		uint32_t b4;
		byte b1[4];
	} u = { 0x00000001 };
	return u.b1[3] == u.b4;
}
/* reverses byte order if not big endian i.e. enforces big endian */
template<typename T>
T big_endian_normalize(T x) {
	if (system_has_big_endian()) return x;
	byte* begin = (byte*)&x;
	byte* end = (byte*)(&x + 1);
	std::reverse(begin, end);
	return x;
}
/* reverses byte order if not big endian i.e. undoes normalization */
template<typename T>
T big_endian_denormalize(T x) {
	//same as normalize
	return big_endian_normalize<T>(x);
}

/* turns dot seperated string address to byte vector e.g. "127.0.0.1" => { 127, 0, 0, 1 } */
std::vector<byte> compress_saddress(const std::string& straddr) {
	std::vector<byte> res;
	size_t dotpos = straddr.find_last_of(':');
	if (dotpos == std::string::npos) dotpos = 0;
	else dotpos++;
	size_t nxtdot = straddr.find_first_of('.');
	if (dotpos == std::string::npos) return res;

	std::string num = straddr.substr(dotpos, nxtdot);
	do {
		res.push_back(stoi(num));
		
		dotpos = nxtdot;
		nxtdot = straddr.find_first_of('.', dotpos + 1);

		num = straddr.substr(dotpos + 1, nxtdot - dotpos - 1);
	} while (dotpos != std::string::npos);

	return res;
}
std::string decompress_byte_address(const std::vector<byte>& byte_addr) {
	std::string res;
	
	for (byte b : byte_addr)
		res += std::to_string(b) + '.';
	res.pop_back();

	return res;
}
std::string stringify_byte_id(const std::vector<byte>& id) {
	std::stringstream ssid;
	ssid << std::hex;
	for (byte b : id) {
		if (b < 0b1111) ssid << 0;
		ssid << (int)b;
	}
	//string id
	return ssid.str();
}

/*
* stun_frame_factory:
* builds stun frames/ messages from scratch
*/

class stun_frame_factory {
	static bool has_seed;
public:
	/* makes entire stun frame */
	static std::vector<byte> generate_frame(const std::vector<byte>& payload , const uint16_t& _12bit_header_method, const byte& _2bit_header_class, const std::vector<byte>& id = std::vector<byte>()) {
		std::vector<byte> frame = generate_header(_12bit_header_method, _2bit_header_class, (uint16_t) payload.size(), id);
		frame.insert(frame.end(), payload.begin(), payload.end());

		return frame;
	}
	/* makes a stun frame header */
	static std::vector<byte> generate_header(const uint16_t& _12bit_method, const byte& _2bit_class, const uint16_t& payload_length, const std::vector<byte>& id = std::vector<byte>()) {
		return generate_header(generate_message_type(_12bit_method, _2bit_class), payload_length, id);
	}

	/* attribute generation encapsulation */
	struct generate_attrib {
		/* makes mapped address attribute */
		static std::vector<byte> mapped_addres(const asio::ip::udp::endpoint& ep) {
			std::string s_address = ep.address().to_string();
			std::vector<byte> byte_address = compress_saddress(s_address);
			uint16_t port = (uint16_t)ep.port();
			bool v4 = ep.address().is_v4();

			//starts with 1 NULL byte
			std::vector<byte> addr(1, NULL);
			//family
			if (v4)	addr.push_back(STUN::ADDRESS::FAMILY::ipv4);
			else	addr.push_back(STUN::ADDRESS::FAMILY::ipv6);
			//port
			push_into_end_of_byte_vector(addr, big_endian_normalize(port));
			//address
			addr.insert(addr.end(), byte_address.begin(), byte_address.end());


			return stun_frame_factory::generate_attrib::basic(STUN::ATTRIB::mapped_address, addr);
		}
		/* makes obfuscated mapped address attribute */
		static std::vector<byte> xor_mapped_addres(const asio::ip::udp::endpoint& ep, const std::vector<byte>& frame_id) {
			std::string s_address = ep.address().to_string();
			std::vector<byte> byte_address = compress_saddress(s_address);
			uint16_t port = (uint16_t)ep.port();
			bool v4 = ep.address().is_v4();

			//starts with 1 NULL byte
			std::vector<byte> addr(1, NULL);

			//family
			if (v4)	addr.push_back(STUN::ADDRESS::FAMILY::ipv4);
			else	addr.push_back(STUN::ADDRESS::FAMILY::ipv6);

			//port
			byte* portptr = (byte*) &port;
			byte* magicptr = (byte*) (&STUN::magic_cookie) + 2;
			//xor on port
			for (size_t i = 0; i < sizeof(port); i++)
				portptr[i] ^= magicptr[i];
			push_into_end_of_byte_vector(addr, big_endian_normalize(port));

			//address
			std::vector<byte> addr_xor_key;
			push_into_end_of_byte_vector(addr_xor_key, big_endian_normalize(STUN::magic_cookie));
			addr_xor_key.insert(addr_xor_key.end(), frame_id.begin(), frame_id.end());
			//xor address
			for (size_t i = 0; i < byte_address.size(); i++)
				byte_address[i] ^= addr_xor_key[i];
			addr.insert(addr.end(), byte_address.begin(), byte_address.end());


			return stun_frame_factory::generate_attrib::basic(STUN::ATTRIB::xor_mapped_address, addr);
		}
		/* makes error_code attribute */
		static std::vector<byte> error_code(const uint16_t& error_code, const std::basic_string<char32_t>& error_reason) {
			if (error_code < 300 || error_code > 699) return std::vector<byte>();
			//starts with 21 NULL bits
			std::vector<byte> error(2, NULL);
			error.push_back((byte) (error_code / 100));
			error.push_back((byte) (error_code % 100));

			for (const char32_t& c : error_reason) {
				const char32_t bec = big_endian_normalize(c);
				byte* b = (byte*) &bec;

				//dropping redundant NULL bytes for proper utf-8 encoding
				for (int i = 0; i < 4; i++)
					if (b[i] != NULL)
						error.push_back(b[i]);
			}

			return stun_frame_factory::generate_attrib::basic(STUN::ATTRIB::error_code, error);
		}
		/* makes unknown_attributes attribute */
		static std::vector<byte> unknown_attributes(const std::vector<uint16_t>& attrib_types) {
			if (attrib_types.empty()) return std::vector<byte>();

			std::vector<byte> big_attrib_types;
			for (const uint16_t& type : attrib_types)
				push_into_end_of_byte_vector(big_attrib_types, big_endian_normalize(type));

			return stun_frame_factory::generate_attrib::basic(STUN::ATTRIB::unknown_attributes, big_attrib_types);
		}

		/* makes attribute from byte vector "value" */
		static std::vector<byte> basic(uint16_t type, std::vector<byte> value) {
			std::vector<byte> attrib;

			uint16_t padding = 4 - (value.size() % 4);
			padding %= 4;

			/* enforced big endian insertion */
			//type
			push_into_end_of_byte_vector(attrib, big_endian_normalize(type));
			//length
			push_into_end_of_byte_vector(attrib, big_endian_normalize((uint16_t) (value.size() + padding) ));
			//value
			attrib.insert(attrib.end(), value.begin(), value.end());

			//adding NULL bytes until size is a multiple of 4
			attrib.insert(attrib.end(), padding, NULL);


			return attrib;
		}
	};
private:
	/* makes a stun frame header */
	static std::vector<byte> generate_header(const uint16_t& message_type, const uint16_t& payload_length, std::vector<byte> id) {
		if (payload_length % 4 != 0) throw "INVALID PAYLOAD length for stun header";

		std::vector<byte> header;

		/* enforced big endian insertion */
		//type
		push_into_end_of_byte_vector(header, big_endian_normalize(message_type));
		//length
		push_into_end_of_byte_vector(header, big_endian_normalize(payload_length));
		//magic cookie
		push_into_end_of_byte_vector(header, big_endian_normalize(STUN::magic_cookie));
		//transaction id
		if (id.size() != 12) id = generate_transaction_id();
		header.insert(header.end(), id.begin(), id.end());
		

		return header;
	}

	/* makes message type from 2 bit class and 12 bit method */
	/* result: 00MM MMMC MMMC MMMM */
	static uint16_t generate_message_type(uint16_t _12bit_method, byte _2bit_class) {
		if (_2bit_class > 0b11) throw "message class must be 2 bit or less";
		if (_12bit_method > 0xFFF) throw "message method must be 12 bit or less";

		//								_12bit_method =	/* 0000 MMMM MMMM MMMM */
		//								_2bit_class =	/* ---- ---- 0000 00CC */
		//method injection
		unsigned short res = 0;							/* 0000 0000 0000 0000 */
		res |= (_12bit_method & 0b1111);				/* 0000 0000 0000 MMMM */
		res |= (_12bit_method & 0b1110000) << 1;		/* 0000 0000 MMM0 MMMM */
		res |= (_12bit_method & 0b111110000000) << 2;	/* 00MM MMM0 MMM0 MMMM */
		//class injection
		res |= (_2bit_class & 1) << 4;					/* 00MM MMM0 MMMC MMMM */
		res |= (_2bit_class & 2) << 7;					/* 00MM MMMC MMMC MMMM */

		return res;
	}

	/* returns 12 bytes (96 bits) of quasi random data */
	static std::vector<byte> generate_transaction_id() {
		if (!has_seed) {
			has_seed = true;
			srand((size_t) time(NULL));
		}
		const size_t sz = 12 / sizeof(short);
		short shrts[sz];
		for (int i = 0; i < sz; i++)
			shrts[i] = rand();

		byte* b = (byte*) shrts;

		std::vector<byte> res;
		res.reserve(12);

		for (int i = 0; i < 12; i++)
			res.push_back(b[i]);

		return res;
	}
};

bool stun_frame_factory::has_seed = false;
