#ifdef _WIN32
#define _WIN32_WINNT 0x0A00
#endif

#include "stun_server.hpp"
#include "stun_frame.hpp"
#include "STUN_ENUMS.hpp"

#define STUN_PORT (unsigned short)3478

void lazy_bind_demo();
void lazy_error_demo();
void end_lazy_demos();

/*
* main:
* runs a stun_server and som basic stun server tests
*/

int main() {
	asio::io_context stun_context;
	asio::io_context::work idle_work(stun_context);
	std::thread stun_thr([&]() { stun_context.run(); });

	stun_server stun_server(stun_context, STUN_PORT);

	lazy_bind_demo();

	//sleeping to sync. server logging with client logging
	std::this_thread::sleep_for(std::chrono::seconds(1));

	lazy_error_demo();

	std::this_thread::sleep_for(std::chrono::seconds(1));
	end_lazy_demos();

	prnt_mtx.lock();
	set_console_color(BLUE);
	std::cout << "\n\n----------\n\n\nTESTS DONE:\n\n\n----------\n\n\n";
	set_console_color(WHITE);
	prnt_mtx.unlock();

	if (stun_thr.joinable()) stun_thr.join();

	return 0;
}

asio::io_context context;
asio::io_context::work idle_work(context);
std::thread thr([&]() { context.run(); });

stun_socket sock(context);
asio::ip::udp::endpoint dst(asio::ip::address().from_string("127.0.0.1"), STUN_PORT);

void end_lazy_demos() {
	context.stop();
	if (thr.joinable()) thr.join();
}

void lazy_error_demo() {
	prnt_mtx.lock();
	set_console_color(BLUE);
	std::cout << "\n\n----------\n\n\nERROR DEMO:\n\n\n";
	set_console_color(WHITE);
	prnt_mtx.unlock();
	

	std::vector<byte> not_stun = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	
	stun_frame unsuported;
	std::vector<byte> attr = stun_frame_factory::generate_attrib::basic(0xFF, std::vector<byte>());
	unsuported.set_header(STUN::HEADER::METHOD::binding, STUN::HEADER::CLASS::request, attr.size());
	unsuported.add_attribute(attr);

	stun_frame error;
	std::vector<byte> ec = stun_frame_factory::generate_attrib::error_code(STUN::ERROR_CODE::bad_request, U"client error testing");
	error.set_header(STUN::HEADER::METHOD::binding, STUN::HEADER::CLASS::error, ec.size());
	error.add_attribute(ec);

	sock.send(not_stun, dst);
	sock.send(unsuported.get_frame(), dst);
	sock.send(error.get_frame(), dst);


	for (int i = 0; i < 3; i++) {
		std::vector<byte> res;
		res = sock.pop_message(true);
		prnt_mtx.lock();
		set_console_color(GREEN);
		stun_frame f(res);

		if (f.get_class() == STUN::HEADER::CLASS::error) {
			std::cout << "error_code:\n";
			if (f.get_n_attribs() > 0) {
				std::vector<byte> er = f.get_attrib_value(0);
				std::cout << "\treason: " << std::string(er.begin() + 4, er.end());
				std::cout << "\n\tclass: " << std::to_string(er[2]) << "\n\tnum:" << std::to_string(er[3]) << '\n';
				if (f.get_n_attribs() > 1) {
					std::vector<byte> unknown = f.get_attrib_value(1);
					for (size_t i = 0; i + 2 <= unknown.size(); i+=2)
						std::cout << "\tunknown attribute value: " << std::to_string(big_endian_denormalize( *(uint16_t*) ((byte*)unknown.data() + i) )) << '\n';
				}
			}
		}
		else {
			set_console_color(RED);
			std::cout << "(unexpected)not error_code\n";
			set_console_color(GREEN);
		}
		prnt_mtx.unlock();
	}
}
void lazy_bind_demo() {
	prnt_mtx.lock();
	set_console_color(BLUE);
	std::cout << "\n\n----------\n\n\nBIND DEMO:\n\n\n";
	set_console_color(WHITE);
	prnt_mtx.unlock();


	stun_frame frame;
	stun_frame frame1;
	frame.set_header(STUN::HEADER::METHOD::binding, STUN::HEADER::CLASS::request, 0);
	frame1.set_header(STUN::HEADER::METHOD::binding, STUN::HEADER::CLASS::request, 0);

	sock.send(frame.get_frame(), dst);
	sock.send(frame1.get_frame(), dst);
	sock.send(frame1.get_frame(), dst);

	for (int i = 0; i < 3; i++) {
		std::vector<byte> res;
		res = sock.pop_message(true);
		prnt_mtx.lock();
		set_console_color(GREEN);
		stun_frame f(res);

		if (f.get_class() == STUN::HEADER::CLASS::success)	std::cout << "binding successful\n";
		else {
			set_console_color(RED);
			std::cout << "(unexpected) binding failed\n";
			set_console_color(GREEN);
		}

		if (f.get_id() == frame.get_id() || f.get_id() == frame1.get_id()) std::cout << "response has expected id\n";
		else {
			set_console_color(RED);
			std::cout << "response has unexpected id\n";
			set_console_color(GREEN);
		}

		std::cout << "id: " << stringify_byte_id(f.get_id()) << '\n'; 
		
		size_t n = f.get_n_attribs();
		if (n > 0) {
			uint16_t type = f.get_attrib_type(0);

			if (type == STUN::ATTRIB::TYPE::mapped_address) {
				std::cout << "ADDRESS\n";
				std::vector<byte> addr_val = f.get_attrib_value(0);
				std::cout << "\tport: " << stun_frame::mapped_address::get_port(addr_val);
				std::cout << "\n\taddress: " << decompress_byte_address(stun_frame::mapped_address::get_address(addr_val));
				std::cout << "\n\tfamily: ";
				if (stun_frame::mapped_address::get_family(addr_val) == STUN::ADDRESS::FAMILY::ipv4) std::cout << "v4\n\n\n";
				else std::cout << "v6\n\n\n";
			}
			else if (type == STUN::ATTRIB::TYPE::xor_mapped_address) {
				std::cout << "XOR ADDRESS\n";
				std::vector<byte> addr_val = f.get_attrib_value(0);
				std::cout << "\tport: " << stun_frame::mapped_address::get_port(addr_val) << "; decoded = [" << stun_frame::xor_mapped_address::get_port(addr_val);
				std::cout << "]\n\taddress: " << decompress_byte_address(stun_frame::mapped_address::get_address(addr_val)) << "; decoded = ["
					<< decompress_byte_address(stun_frame::xor_mapped_address::get_address(addr_val, f.get_id()));
				std::cout << "]\n\tfamily: ";
				if (stun_frame::xor_mapped_address::get_family(addr_val) == STUN::ADDRESS::FAMILY::ipv4) std::cout << "v4\n\n\n";
				else std::cout << "v6\n\n\n";
			}
		}

		set_console_color(WHITE);
		prnt_mtx.unlock();
	}

	stun_frame keep_alive_indication;
	keep_alive_indication.set_header(STUN::HEADER::METHOD::binding, STUN::HEADER::CLASS::indication, 0);

	std::this_thread::sleep_for(std::chrono::milliseconds(500));

	sock.send(keep_alive_indication.get_frame(), dst);
}
