#pragma once

#include <mutex>

/*
* s_:
* thread safe wrapper
* for primitive types
*/

template <typename T>
class s_ {
	T t;
	std::mutex mtx;
public:
	s_() : t() { }
	s_(T t) : t(t) { }
	s_(T& t) : t(t) { }
	s_(T&& t) : t(t) { }

	T operator!() {
		std::unique_lock<std::mutex> lk(mtx);
		return !t;
	}
	T operator[](const size_t& i) {
		std::unique_lock<std::mutex> lk(mtx);
		return t[i];
	}

	T operator=(const T& x) {
		std::unique_lock<std::mutex> lk(mtx);
		t = x;
		return t;
	}
	T operator-(const T& x) {
		std::unique_lock<std::mutex> lk(mtx);
		t -= x;
		return t;
	}
	T operator+(const T& x) {
		std::unique_lock<std::mutex> lk(mtx);
		t += x;
		return t;
	}
	T operator*(const T& x) {
		std::unique_lock<std::mutex> lk(mtx);
		t *= x;
		return t;
	}
	T operator/(const T& x) {
		std::unique_lock<std::mutex> lk(mtx);
		t /= x;
		return t;
	}
};
