#pragma once

/*
* STUN:
* stun related constants
*/
struct STUN {
	static const uint32_t magic_cookie = 0x2112A442;

	struct HEADER {
		enum CLASS {
			request, indication, success, error
		};
		enum METHOD {
			binding = 1
		};
	};

	enum ERROR_CODE {
		//malformed
		bad_request = 400,
		unknown_attribute = 420,
		server_error = 500
	};

	struct ATTRIB {
		enum TYPE {
			mapped_address = 0x1,
			//username = 0x6,
			//message_integrity = 0x8,
			error_code = 0x9,
			unknown_attributes = 0xA,
			//realm = 0x14,
			//nonce = 0x15,
			xor_mapped_address = 0x20
		};
	};

	struct ADDRESS {
		enum FAMILY {
			ipv4 = 1, ipv6
		};
	};
};
