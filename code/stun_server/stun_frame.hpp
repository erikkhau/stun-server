#pragma once
#include "stun_frame_factory.hpp"
#include "STUN_ENUMS.hpp"

/*
* stun_frame:
* helps decode stun messages
*/

class stun_frame {
	//stun frame components
	std::vector<byte> header;
	std::vector<std::vector<byte>> attributes;
public:
	stun_frame() {

	}
	stun_frame(const std::vector<byte>& frame) {
		set_frame(frame);
	}

	/* return codes: */
	/* 0: valid format */
	/* 1: missing data */
	/* 3: invalid format */
	static uint8_t check_stun_frame(const std::vector<byte>& frame) {
		/* missing data */
		if (frame.size() < 20)		return 1;



		/* invalid format */
		if (frame[0] > 0b111111)			return 3;

		uint16_t length = big_endian_denormalize(*(uint16_t*) (frame.data() + 2));
		if (frame.size() - 20 != length)	return 3;

		uint32_t cookie = big_endian_denormalize(*(uint32_t*)(frame.data() + 4));
		if (cookie != STUN::magic_cookie)	return 3;



		/* missing data */
		if (frame.size() % 4 != 0)	return 1;



		/* valid format */
		return 0;
	}
	/* gets length encoded in stun header or attribute */
	static uint16_t get_stun_size(const std::vector<byte>& attrib_or_header, size_t begining = 0) {
		if (attrib_or_header.size() < 4) return 0;
		return big_endian_denormalize(*(uint16_t*)(attrib_or_header.data() + 2 + begining));
	}


	void set_header(const uint16_t& _12bit_method, const byte& _2bit_class, const uint16_t& payload_length) {
		header = stun_frame_factory::generate_header(_12bit_method, _2bit_class, payload_length);
	}
	void add_attribute(const uint16_t& type, const std::vector<byte>& value) {
		attributes.push_back( stun_frame_factory::generate_attrib::basic(type, value) );
	}
	void add_attribute(const std::vector<byte>& attrib) {
		attributes.push_back(attrib);
	}
	/* naive */
	void set_frame(const std::vector<byte>& frame) {
		if (frame.size() < 20) return;

		header.insert(header.begin(), frame.begin(), frame.begin() + 20);

		attributes.clear();
		size_t pos = 20;
		while (pos < frame.size()) {
			size_t attrib_sz = get_stun_size(frame, pos) + 4;
			add_attribute(std::vector<byte> (frame.begin() + pos, frame.begin() + pos + attrib_sz));
			pos += attrib_sz;
		}
	}

	std::vector<byte> get_frame() const {
		if (header.size() != 20) return std::vector<byte>();

		std::vector<byte> frame = header;

		for (const std::vector<byte>& attrib : attributes)
			frame.insert(frame.end(), attrib.begin(), attrib.end());

		return frame;
	}

	/* returns 0xFFFF (uint16_t max) if not found or invalid */
	uint16_t get_method() const {
		if (header.size() < 20) return 0xFFFF;

		uint16_t type = *(uint16_t*)header.data();
		type = big_endian_denormalize(type);		/* 00MM MMMC MMMC MMMM */

		uint16_t method = 0;						/* 0000 0000 0000 0000 */
		method |= (type & 0b1111);					/* 0000 0000 0000 MMMM */
		method |= (type & 0b11100000) >> 1;			/* 0000 0000 0MMM MMMM */
		method |= (type & 0b11111000000000) >> 2;	/* 0000 MMMM MMMM MMMM */

		return method;
	}
	/* returns 0xFF (uint8_t max) if not found or invalid */
	uint8_t get_class() const {
		if (header.size() < 20) return 0xFF;

		uint16_t type = *(uint16_t*)header.data();
		type = big_endian_denormalize(type);	/* 00MM MMMC MMMC MMMM */

		uint8_t _class = 0;						/* 0000 0000 */
		_class |= (type & 0b10000) >> 4;		/* 0000 000C */
		_class |= (type & 0b100000000) >> 7;	/* 0000 00CC */

		return _class;
	}
	/* returns 0 bytes if not found or invalid */
	/* and 12 bytes (96 bits) if successful */
	std::vector<byte> get_id() const {
		if (header.size() < 20) return std::vector<byte>();

		auto idbegin = header.begin() + 8;
		auto idend = header.begin() + 20;

		std::vector<byte> id;
		id.insert(id.end(), idbegin, idend);

		return id;
	}
	/* returns number of attributes */
	size_t get_n_attribs() const {
		return attributes.size();
	}
	uint16_t get_attrib_type(size_t index) const {
		const std::vector<byte>& a = attributes[index];
		if (a.size() < 2) return 0xFFFF;
		uint16_t* type = (uint16_t*) a.data();
		return big_endian_denormalize(*type);
	}
	std::vector<byte> get_attrib_value(size_t index) const {
		const std::vector<byte>& a = attributes[index];
		if (a.size() < 8) return std::vector<byte>();

		return std::vector<byte> (a.begin() + 4, a.end());
	}

	struct mapped_address {
		static uint8_t get_family(const std::vector<byte>& mapped_address) {
			if (mapped_address.size() != 8 && mapped_address.size() != 20) return 0xFF;

			return mapped_address[1];
		}
		static uint16_t get_port(const std::vector<byte>& mapped_address) {
			if (mapped_address.size() != 8 && mapped_address.size() != 20) return 0xFFFF;

			uint16_t* port = (uint16_t*) &mapped_address[2];
			return big_endian_denormalize(*port);
		}
		static std::vector<byte> get_address(const std::vector<byte>& mapped_address) {
			if (mapped_address.size() != 8 && mapped_address.size() != 20) return std::vector<byte> ();

			return std::vector<byte> (mapped_address.begin() + 4, mapped_address.end());
		}
	};
	struct xor_mapped_address {
		static uint8_t get_family(const std::vector<byte>& mapped_address) {
			return stun_frame::mapped_address::get_family(mapped_address);
		}
		static uint16_t get_port(const std::vector<byte>& mapped_address) {
			uint16_t port = stun_frame::mapped_address::get_port(mapped_address);

			byte* portptr = (byte*)&port;
			byte* magicptr = (byte*)(&STUN::magic_cookie) + 2;
			for (size_t i = 0; i < sizeof(port); i++)
				portptr[i] ^= magicptr[i];

			return port;
		}
		static std::vector<byte> get_address(const std::vector<byte>& mapped_address, const std::vector<byte>& frame_id) {
			std::vector<byte> byte_address = stun_frame::mapped_address::get_address(mapped_address);

			std::vector<byte> addr_xor_key;
			push_into_end_of_byte_vector(addr_xor_key, big_endian_denormalize(STUN::magic_cookie));
			addr_xor_key.insert(addr_xor_key.end(), frame_id.begin(), frame_id.end());
			//xor address
			for (size_t i = 0; i < byte_address.size(); i++)
				byte_address[i] ^= addr_xor_key[i];
			
			return byte_address;
		}
	};
};
