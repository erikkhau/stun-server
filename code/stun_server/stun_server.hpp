#pragma once

#include "s_list.hpp"
#include "stun_socket.hpp"
#include "stun_frame.hpp"

#ifdef _WIN32
#include <Windows.h>
#define WHITE (short)7
#define BLUE (short)9
#define GREEN (short)10
#define CYAN (short)11
#define RED (short)12
#define YELLOW (short)14
#else
#define WHITE (short)0
#define BLUE (short)0
#define GREEN (short)0
#define CYAN (short)0
#define RED (short)0
#define YELLOW (short)0
#endif

//for cout
std::mutex prnt_mtx;

/*
*	set_console_color:
*	works only for windows
*/

void set_console_color(short k) {
#ifdef _WIN32
	HANDLE hconsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hconsole, (WORD)k);
#endif
}

#define INVALID_UDP_ENDPOINT asio::ip::udp::endpoint(asio::ip::address().from_string("0.0.0.0"), 0)

/*
* stun_server:
* 
* supports binding requests and indications.
* detects retransmissions and malfromed stun messages.
* responds with stun error upon reading unknown attributes.
*/

class stun_server {
	asio::io_context& m_context;
	stun_socket m_sock;

	s_list<stun_socket> m_remote_cons;

	//stores last 10 transaction ids
	s_deque<std::vector<byte>> handled_ids;
public:
	stun_server(asio::io_context& context, unsigned short port) :
		m_context(context),
		m_sock(context, asio::ip::udp::endpoint(asio::ip::udp::v6(), port))
	{
		m_sock.on_message([&] (const std::vector<byte>& payload, const asio::ip::udp::endpoint& src) { handle_payload(payload, src); });
	}
	~stun_server() {

	}


private:
	/* handles server data input */
	void handle_payload(const std::vector<byte>& payload, const asio::ip::udp::endpoint& src) {
		uint8_t f_ec = stun_frame::check_stun_frame(payload);
		stun_frame f(payload);
		std::vector<byte> id = f.get_id();
		
		if (handled_ids.contains(id)) {
			prnt("(retransmission) msg_id: " + stringify_byte_id(id), YELLOW);	//TODO: respond with copy of original response
		}
		else {
			handled_ids.push_back(id);
			if (handled_ids.size() > 10) handled_ids.pop_front();
		}

		if (f_ec) {
			prnt("(malformed)", m_sock.local_endpoint(), src, RED);

			std::vector<byte> error = stun_frame_factory::generate_attrib::error_code(STUN::ERROR_CODE::bad_request, U"insufficiently magic cookie (malformed or not STUN)");

			write_stun_response(STUN::HEADER::METHOD::binding, STUN::HEADER::CLASS::error, error, src, id);
		}
		else {
			handle_stun_frame(f, src);
		}
	}
	/* deals with verified stun frames, and sends them to relevant handler based on stun frame header method */
	void handle_stun_frame(const stun_frame& f, const asio::ip::udp::endpoint& src) {
		uint16_t header_method = f.get_method();

		if (header_method == STUN::HEADER::METHOD::binding) {
			handle_stun_frame_binding(f, src);
		}
	}
	/* deals with recieved binding methods */
	void handle_stun_frame_binding(const stun_frame& f, const asio::ip::udp::endpoint& src) {
		uint8_t header_class = f.get_class();

		//id
		std::vector<byte> id = f.get_id();
		std::string sid = stringify_byte_id(id);

		//attrib checker
		std::vector<uint16_t> unknown_attribs;
		for (size_t i = 0; i < f.get_n_attribs(); i++) {
			uint16_t type = f.get_attrib_type(i);
			//client sent error_code
			if (type == STUN::ATTRIB::TYPE::error_code) {
				prnt("(error_code received)", m_sock.local_endpoint(), src, RED);

				std::vector<byte> error = stun_frame_factory::generate_attrib::error_code(STUN::ERROR_CODE::server_error, U"not my problem (received error_code)");

				write_stun_response(STUN::HEADER::METHOD::binding, STUN::HEADER::CLASS::error, error, src, id);
				return;
			}
			//note comprehension required attribs without support
			else if (type < 0x7FFF) {
				unknown_attribs.push_back(type);
			}
			else {
				/* can ignore */
			}
		}

		//comprehension required attribs without suport detected
		if (!unknown_attribs.empty()) {
			std::vector<byte> error = stun_frame_factory::generate_attrib::error_code(STUN::ERROR_CODE::unknown_attribute, U"what? (comprehension required attributes aren't supported)");
			std::vector<byte> unknattr = stun_frame_factory::generate_attrib::unknown_attributes(unknown_attribs);
			error.insert(error.end(), unknattr.begin(), unknattr.end());

			write_stun_response(STUN::HEADER::METHOD::binding, STUN::HEADER::CLASS::error, error, src, id);
			return;
		}

		//BINDING REQUEST
		if (header_class == STUN::HEADER::CLASS::request) {
			prnt("(binding request) msg_id: " + sid, m_sock.local_endpoint(), src, CYAN);

			std::vector<byte> xor_mapped_address = stun_frame_factory::generate_attrib::xor_mapped_addres(src, id);

			
			write_stun_response(STUN::HEADER::METHOD::binding, STUN::HEADER::CLASS::success, xor_mapped_address, src, id);
		}
		//BINDING INDICATION
		else if (header_class == STUN::HEADER::CLASS::indication) {
			prnt("(binding indication) msg_id: " + sid, m_sock.local_endpoint(), src);
		}
		//BINDING SUCCESS
		else if (header_class == STUN::HEADER::CLASS::success) {
			/* shouldn't happen */
		}
		//BINDING ERROR
		else if (header_class == STUN::HEADER::CLASS::error) {
			/* shouldn't happen */
		}
	}

	/* writes a response to "dst" */
	/* NB: doesn't check validity of "attribs" */
	void write_stun_response(const uint16_t& _12bit_method, const byte& _2bit_class, const std::vector<byte>& attribs, const asio::ip::udp::endpoint& dst, const std::vector<byte>& id = std::vector<byte>()) {
		std::vector<byte> frame = stun_frame_factory::generate_header(_12bit_method, _2bit_class, (uint16_t)attribs.size(), id);
		frame.insert(frame.end(), attribs.begin(), attribs.end());

		m_sock.send(frame, dst);
	}

	void prnt(const std::string& info, short color = WHITE) { prnt(info, INVALID_UDP_ENDPOINT, INVALID_UDP_ENDPOINT, color); }
	void prnt(const std::string& info, const asio::ip::udp::endpoint& local, const asio::ip::udp::endpoint& remote, short color = WHITE) {
		bool has_endpoint = local.port() != NULL || remote.port() != NULL;
		std::string endpoint_info;
		if (has_endpoint)			endpoint_info = "{ ";
		if (local.port() != NULL)	endpoint_info += "local: "	+ std::to_string(local.port()) + " ";
		if (remote.port() != NULL)	endpoint_info += "remote: "	+ std::to_string(remote.port()) + " ";
		if (has_endpoint)			endpoint_info += "} ";


		std::unique_lock<std::mutex> prnt_lk(prnt_mtx);
		set_console_color(color);
		std::cout << "[STUN SERVER] " << endpoint_info << info << std::endl;
		set_console_color(WHITE);
	}
};
