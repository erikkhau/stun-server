//has all the connected peers
const connections = {};


const sock = io.connect(window.location.origin);

//saves the other peers localDescription as remoteDescription
sock.on("answer", (id, description) => {
  connections[id].setRemoteDescription(description);
});

//when a new peer connects this function is run,
//gets public ip from localhost:3478,
//creates offer and sets everything up
sock.on("watcher", id => {
  const peerConnection = new RTCPeerConnection({
  iceServers: [
    {
      urls: 'stun:localhost:3478',
    },
  ],
});
  connections[id] = peerConnection;

  let stream = vid.srcObject;
  stream.getTracks().forEach(track => peerConnection.addTrack(track, stream));

  peerConnection.onicecandidate = event => {
    if (event.candidate) {
      sock.emit("candidate", id, event.candidate);
    }
  };

  peerConnection
    .createOffer()
    .then(sdp => peerConnection.setLocalDescription(sdp))
    .then(() => {
      sock.emit("offer", id, peerConnection.localDescription);
    });
});

sock.on("candidate", (id, candidate) => {
  connections[id].addIceCandidate(new RTCIceCandidate(candidate));
});


//cloeses a connection when a peer disconncects
sock.on("disconnectPeer", id => {
  connections[id].close();
  delete connections[id];
});


//closes the socket when a window is closed
window.onunload = window.onbeforeunload = () => {
  sock.close();
};

// Handles all the video and audio related stuff
//such as camera and microphone accses
const vid = document.querySelector("video");
const aud_sel = document.querySelector("select#audioSource");
const vid_sel = document.querySelector("select#videoSource");

 aud_sel.onchange = getStream;
vid_sel.onchange = getStream;

getStream()
  .then(getDevices)
  .then(gotDevices);

function getDevices() {
  return navigator.mediaDevices.enumerateDevices();
}

function gotDevices(deviceInfos) {
  window.deviceInfos = deviceInfos;
  for (const deviceInfo of deviceInfos) {
    const opt = document.createElement("option");
    opt.value = deviceInfo.deviceId;
    if (deviceInfo.kind === "audioinput") {
     opt.text = deviceInfo.label || `Microphone ${ aud_sel.length + 1}`;
       aud_sel.appendChild(opt);
    } else if (deviceInfo.kind === "videoinput") {
      opt.text = deviceInfo.label || `Camera ${vid_sel.length + 1}`;
      vid_sel.appendChild(opt);
    }
  }
}

function getStream() {
  if (window.stream) {
    window.stream.getTracks().forEach(track => {
      track.stop();
    });
  }
  const aud_src =  aud_sel.value;
  const vid_src = vid_sel.value;
  const constraints = {
    audio: { deviceId: aud_src ? { exact: aud_src } : undefined },
    video: { deviceId: vid_src ? { exact: vid_src } : undefined }
  };
  return navigator.mediaDevices
    .getUserMedia(constraints)
    .then(gotStream)
    .catch(handleError);
}

function gotStream(stream) {
  window.stream = stream;
   aud_sel.selectedIndex = [... aud_sel.options].findIndex(
    opt => opt.text === stream.getAudioTracks()[0].label
  );
  vid_sel.selectedIndex = [...vid_sel.options].findIndex(
    opt => opt.text === stream.getVideoTracks()[0].label
  );
  vid.srcObject = stream;
  sock.emit("broadcaster");
}

function handleError(error) {
  console.error("Error: ", error);
}