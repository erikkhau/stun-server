let connection;

const sock = io.connect(window.location.origin);
const vid = document.querySelector("video");
const audio = document.querySelector("#enable-audio");

audio.addEventListener("click", enableAudio)

//sets everything up when a peer connects to a broadcaster.
//gets public ip form the stun server at localhost:3478
sock.on("offer", (id, description) => {
  connection = new RTCPeerConnection({
  iceServers: [
    {
      urls: 'stun:localhost:3478',
    },
  ],
});
  connection
    .setRemoteDescription(description)
    .then(() => connection.createAnswer())
    .then(sdp => connection.setLocalDescription(sdp))
    .then(() => {
     sock.emit("answer", id, connection.localDescription);
    });
  connection.ontrack = event => {
    vid.srcObject = event.streams[0];
  };
 connection.onicecandidate = event => {
    if (event.candidate) {
      sock.emit("candidate", id, event.candidate);
    }
  };
});

//handles a new candidate
sock.on("candidate", (id, candidate) => {
  connection
    .addIceCandidate(new RTCIceCandidate(candidate))
    .catch(e => console.error(e));
});

sock.on("connect", () => {
  sock.emit("watcher");
});

sock.on("broadcaster", () => {
  sock.emit("watcher");
});

//closes the connection and socket when the window is closed
window.onunload = window.onbeforeunload = () => {
  sock.close();
  connection.close();
};

//unmutes audio
function enableAudio() {
  console.log("Enabling audio")
  vid.muted = false;
}